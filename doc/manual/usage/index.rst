.. _basic_usage:

============
Introduction
============

If you are new to OVITO, you can start by reading the following sections of this manual:

.. toctree::
  :maxdepth: 1
  :includehidden:

  import
  viewports
  data_model
  pipeline
  rendering
  miscellaneous
  export
