.. _application_settings.viewports:

Viewport settings
=================

.. image:: /images/app_settings/viewport_settings.*
  :width: 45%
  :align: right

This page of the :ref:`application settings dialog <application_settings>`
contains options related to the interactive viewports of the OVITO.

Camera
""""""

Coordinate system orientation
  OVITO can restrict the viewport camera rotation such that the selected Cartesian coordinate axis
  always points upward. Default: z-axis.

Restrict camera rotation to keep the major axis pointing upward
  This option constrains the camera's orientation to prevent the camera from turning upside down.

Viewport background
"""""""""""""""""""

This option changes between a dark (default) and a white viewport background.

.. _application_settings.viewports.graphics_implementation:

Viewport 3D graphics
""""""""""""""""""""

Click on the "Configure..." button to open the :ref:`real-time graphics configuration <viewports.configure_graphics_dialog>` dialog,
which is also accessible via the :ref:`viewports context menu <usage.viewports.menu>` in the main window.